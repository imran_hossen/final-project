<button><a href="index.php" type="button">Go to List page</a></button></br></br>

<?php

session_start();
error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Mobile\Mobile;
use App\BITM\SEIP113286\Utility\Utility;

$mobiles = new Mobile();
$AllMobile = $mobiles->trashed();

$obj = new Utility();
//$obj->debug($AllMobile);

if(isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}



?>
<head>
    <title>Mobile Models | Mobile List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <?php
    if(isset($AllMobile) && !empty($AllMobile)){
        $serial = 0;
        foreach ($AllMobile as $Mobile){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Mobile['id'] ?>
                </td>
                <td>
                    <?php echo $Mobile['title'] ?>
                </td>
                <td>
                    <a href="restore.php?id=<?php echo $Mobile['id'] ?>">Restore</a> |
                    <a href="delete.php?id=<?php echo $Mobile['id'] ?>">Clean Permanent</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
            <td>
            </td>
        </tr>
    <?php  }
    ?>
</table>
