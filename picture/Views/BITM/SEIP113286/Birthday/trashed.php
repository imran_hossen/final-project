<button><a href="create.php" type="button">Add new model</a></button> | <button><a href="index.php" type="button">List Page</a></button></br></br>

<?php

error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Birthday\Birthday;

$obj = new Birthday();
$AllData = $obj->trashed();


?>
<head>
    <title>Birthdate | Delete List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Name</th>
        <th>Birthdate</th>
        <th>Action</th>
    </tr>
    <?php
    if(isset($AllData) && !empty($AllData)){
        $serial = 0;
        foreach ($AllData as $Data){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Data['id'] ?>
                </td>
                <td>
                    <?php echo $Data['title'] ?>
                </td>
                <td>
                    <?php echo $Data['date'] ?>
                </td>
                <td>
                    <a href="restore.php?id=<?php echo $Data['id'] ?>">Restore</a> |
                    <a href="delete.php?id=<?php echo $Data['id'] ?>">Clean</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
        </tr>
    <?php  }
    ?>
</table>
