<?php
error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Birthday\Birthday;
$id = $_GET['id'];
$datas = new Birthday();
$data = $datas->show($id);

?>
<head>
    <title>Birthdate | Single Show</title>
</head>
<button><a href="create.php" type="button">Add new model</a></button> |
<button><a href="index.php" type="button">Back to List</a></button>
<br/><br/>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Date</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $data['id']?></td>
        <td><?php echo $data['title']?></td>
        <td><?php echo $data['date']?></td>
        <td>
            <a href="edit.php?id=<?php echo $id ?>">Edit</a> |
            <a href="trash.php?id=<?php echo $id ?>">Delete</a>
        </td>
    </tr>
</table>