<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Actors\Actors;
$obj = new Actors();
$id = $_GET['id'];
$data = $obj->show($id);
?>

<head>
    <title>Favourit Actors | Show</title>
</head>
<button><a href="create.php" type="button">Add new Hobbies</a></button> |
<button><a href="index.php" type="button">Back to List</a></button>
<br/><br/>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Actors</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $data['id']?></td>
        <td><?php echo $data['title']?></td>
        <td><?php echo $data['actors']?></td>
        <td><?php echo $data['created_at']?></td>
        <td><?php echo $data['updated_at']?></td>
        <td>
            <a href="edit.php?id=<?php echo $id ?>">Edit</a> |
            <a href="trash.php?id=<?php echo $id ?>">Delete</a>
        </td>
    </tr>
</table>