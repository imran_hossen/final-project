<button><a href="index.php" type="button">View Slide</a></button> | <button><a href="create.php" type="button">Create new +</a></button> | <button><a href="trashed.php" type="button">Deleted data</a></button></br></br>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Slider\Slider;

$obj = new Slider();
$Datas = $obj->slider_list();

?>
<head>
    <title>Slider | List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th>Subtitle</th>
        <th>Slider Picture</th>
        <th>Action</th>
    </tr>
    <?php
    if(isset($Datas) && !empty($Datas)){
        $serial = 0;
        foreach ($Datas as $Data){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Data['id'] ?>
                </td>
                <td>
                    <?php echo $Data['title'] ?>
                </td>
                <td>
                    <?php echo $Data['subtitle'] ?>
                </td>
                <td>
                    <img src="<?php echo "../../../../assets/img/Slider/".$Data['image'] ?>" width="200" height="150"/>
                </td>
                <td>
                    <a href="edit.php?id=<?php echo $Data['id'] ?>">Edit</a> |
                    <a href="show.php?id=<?php echo $Data['id'] ?>">Show</a> |
                    <?php
                    if ($Data['active']==='no')
                    {
                        echo '<a href="active.php?id=<?php echo $Data['id']" ?>Active</a>';
                    }
                    else-->
                    {
                        echo "<a href="deactive.php?id=--><?php //echo $Data['id'] ?><!--">Deactive |</a>}-->
                    ?>-->
                    <a href="trash.php?id=<?php echo $Data['id'] ?>">Delete</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
            <td>
            </td>
        </tr>
    <?php  }
    ?>
</table>



