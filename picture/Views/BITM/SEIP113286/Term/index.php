<button><a href="create.php" type="button">Create new +</a></button> | <button><a href="trashed.php" type="button">Deleted data</a></button></br></br>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Term\Term;

$obj = new Term();
$Datas = $obj->index();

?>
<head>
    <title>Term and condition | List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Name</th>
        <th>Term</th>
        <th>Action</th>
    </tr>
    <?php
    if(isset($Datas) && !empty($Datas)){
        $serial = 0;
        foreach ($Datas as $Data){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Data['id'] ?>
                </td>
                <td>
                    <?php echo $Data['title'] ?>
                </td>
                <td>
                    <?php if($Data['term']==1){echo "Checked";}else{echo "Unchecked";} ?>
                </td>
                <td>
                    <a href="edit.php?id=<?php echo $Data['id'] ?>">Edit</a> |
                    <a href="show.php?id=<?php echo $Data['id'] ?>">Show</a> |
                    <a href="trash.php?id=<?php echo $Data['id'] ?>">Delete</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
            <td>
            </td>
        </tr>
    <?php  }
    ?>
</table>



