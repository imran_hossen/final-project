<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Term\Term;
$obj = new Term();
$id = $_GET['id'];
$data = $obj->show($id);
?>

<head>
    <title>Term and conditon | Show</title>
</head>
<button><a href="create.php" type="button">Add new model</a></button> |
<button><a href="index.php" type="button">Back to List</a></button>
<br/><br/>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Term</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $data['id']?></td>
        <td><?php echo $data['title']?></td>
        <td><?php echo $data['term']?></td>
        <td><?php echo $data['created_at']?></td>
        <td><?php echo $data['updated_at']?></td>
        <td>
            <a href="edit.php?id=<?php echo $id ?>">Edit</a> |
            <a href="trash.php?id=<?php echo $id ?>">Delete</a>
        </td>
    </tr>
</table>