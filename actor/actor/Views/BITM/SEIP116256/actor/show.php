<?php

include_once '../../../../Src/BITM/SEIP116256/actor/actor.php';
use App\BITM\SEIP116256\actor\actor;
$obj = new actor();
$id = $_GET['id'];
$data = $obj->show($id);
?>

<head>
    <title>actor's | Show</title>
</head>
<a href="create.php" >Add new actor's</a>
<a href="index.php">Back to List</a>
<br/><br/>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>actor</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $data['id']?></td>
        <td><?php echo $data['title']?></td>
        <td><?php echo $data['actor']?></td>
        <td><?php echo $data['created_at']?></td>
        <td><?php echo $data['edited_at']?></td>
        <td>
            <a href="edit.php?id=<?php echo $id ?>">Edit</a> |
            <a href="trash.php?id=<?php echo $id ?>">Delete</a>
        </td>
    </tr>
</table>