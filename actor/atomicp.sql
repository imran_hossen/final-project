-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2016 at 11:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicp`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `actor` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`id`, `title`, `actor`, `created_at`, `edited_at`, `deleted_at`) VALUES
(1, 'hhhh', 'imran', '2016-03-29 11:10:29', '0000-00-00 00:00:00', '2016-03-29 11:54:31'),
(2, 'tttt', 'prodip', '2016-03-29 11:17:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'yyyyyyyyk', 'imran,sumon,prodip,mahfuz', '2016-03-29 11:17:55', '2016-03-29 11:53:59', '0000-00-00 00:00:00'),
(4, 'ppppppp', 'mahfuz', '2016-03-29 11:18:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'iiii', 'imran,khurshed', '2016-03-29 11:19:49', '2016-03-29 11:54:06', '0000-00-00 00:00:00'),
(6, 'kjahjsad', 'prodip', '2016-03-29 11:54:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'bangla', 'prodip', '2016-03-29 11:54:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actor`
--
ALTER TABLE `actor`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actor`
--
ALTER TABLE `actor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
