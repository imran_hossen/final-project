<?php
include_once '../../../../header.php';
session_start();
//print_r($_SESSION);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Hobby | Create Page
        </title>
    </head>
    <body>
        <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> |
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> 
                    


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        
        <form action="store.php" method="POST" >
            <fieldset style="text-align: left;">
                <legend>Add Hobby Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name">
                <span>
                    <?php
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            unset($_SESSION['namerr']);
                        }
                    ?>
                </span>
                <br>
                <label>Select Hobbies:</label>
                <input type="checkbox" name="hobby[]" value="Cricket">Cricket
                <input type="checkbox" name="hobby[]" value="Football">Football
                <input type="checkbox" name="hobby[]" value="Reading">Reading
                <input type="checkbox" name="hobby[]" value="Gardening">Gardening
                <span>
                    <?php
                        if(isset($_SESSION['hobbyerr'])){
                            echo $_SESSION['hobbyerr'];
                            unset($_SESSION['hobbyerr']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        </div>
        
    </body>
</html>