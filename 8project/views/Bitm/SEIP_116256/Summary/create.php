<?php
include_once '../../../../header.php';
session_start();
//var_dump($_SESSION);
?>
<html>
    <head>
        <title>
            Summary | Create Page
        </title>
    </head>
    <body>
         <h1 align="center">Create Page</h1><hr>
        <div class="" style="margin-bottom: 20px;">
            <div class="row">
                <div class="" align="center" style="font-size: 22px;">
                    <a href="create.php">
                        <button type="button" class="btn btn-primary">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Add New
                        </button>
                    </a> |
                    <a href="index.php">
                        <button type="button" class="btn btn-info">
                            <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                            Home
                        </button>
                    </a> 


                </div>
            </div>
        </div>

        <div class="content" align="center" style="font-size: 22px; margin-bottom: 100px;">
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Organization INFO</legend>
                <table>
                    <tr>
                        <td><label>Organization Name</label></td>
                        <td><input type="text" name="name" id="name"></td>
                        <td>
                            <?php 
                                if(isset($_SESSION['namerr'])){
                                echo $_SESSION['namerr'];
                                unset($_SESSION['namerr']);
                                }
                            ?>
                        </td>
                </tr>
                <tr>
                    <td><label>Enter Summary</label></td>
                    <td><textarea name="summary" rows="6" cols="40"></textarea></td>
                    <td>
                            <?php 
                                if(isset($_SESSION['emailerr'])){
                                echo $_SESSION['emailerr'];
                                unset($_SESSION['emailerr']);
                                }
                            ?>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="3">
                        <div class="pull-right">
                        <input type="submit"  value="Save">
                        <input type="reset"  value="Reset">
                        </div>
                    </td>
                </tr>
                </table>
            </fieldset>
        </form>
        </div>
    </body>
</html>
