<!DOCTYPE html>
<html>
    <head>
        <title>
            BITM Atomic Projects | Home Page
        </title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/custom.css">
    </head>
    <body>

        <div class="container">
            <div class="" style="background-color: #01A9DB;">
                <div class="row">
                    <div class="col-md-3">
                        <img src="img/logo.png" height="80" style="margin: 40px 0 0 65px;"> 
                    </div>
                    <div class="col-md-9">
                        <img src="img/project_banner.jpg" width="100%" height="170">
                    </div>
                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="views/Bitm/SEIP_116256/Book/index.php">Book</a></li>
                            <li><a href="views/Bitm/SEIP_116256/Mobile/index.php">Mobile</a></li> 
                            <li><a href="views/Bitm/SEIP_116256/Birthday/index.php">Birthday</a></li> 
                            <li><a href="views/Bitm/SEIP_116256/Gender/index.php">Gender</a></li> 
                            <li><a href="views/Bitm/SEIP_116256/Hobby/index.php">Hobby</a></li> 
                            <li><a href="views/Bitm/SEIP_116256/ProfilePicture/index.php">Profile Picture</a></li> 
                            <li><a href="views/Bitm/SEIP_116256/Subscription/index.php">Subcription</a></li> 
                            <li><a href="views/Bitm/SEIP_116256/Summary/index.php">Summary</a></li> 
                            
                       
                    </div>
                </div>
            </nav>

            <div class="row">
                <div class="col-md-8">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="img/design_banner.jpg" alt="Chania">
                                <div class="carousel-caption">
                                    
                                </div>
                            </div>

                            <div class="item">
                                <img src="img/banner.jpg" alt="Chania">
                                <div class="carousel-caption">
                                    
                                </div>
                            </div>


                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                    <div class="panel" style="margin-top: 25px;">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1415261415.jpg" alt="...">
                                <div class="caption">
                                    <h3>Projects of BITM</h3>
                                    <p>BASIS, through Skills for Employment Investment Program (SEIP) will to train up 23,000 over the contract period of 3 years. 5000, 9000 and 9000 trainees will be trained in the 1st, 2nd and 3rd year respectively. BASIS institute of Technology & Management (BITM) is planning to proposed twelve courses for new entrants and two courses for up skilling programs under this SEIP project. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1415261370.jpg" alt="...">
                                <div class="caption">
                                    <h3>Registration for SEIP Project</h3>
                                    <p>BASIS, through Skills for Employment Investment Program (SEIP) will to train up 23,000 over the contract period of 3 years. 5000, 9000 and 9000 trainees will be trained in the 1st, 2nd and 3rd year respectively. BASIS institute of Technology & Management (BITM) is planning to proposed twelve courses for new entrants and two courses for up skilling programs under this SEIP project</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="img/1416294158.jpg" alt="...">
                                <div class="caption">
                                    <h3>Upcoming Training</h3>
                                    <p>To address the skill gap of HR in the industry, BASIS started its own training activities in 2007. Later in 2012, BASIS institutionalized its training activities and set up BASIS Institute of Technology & Management (BITM) </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>


                </div>




                <div class="col-md-4">
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>






        </div>

        
        
        <footer>
            <div class="container" style=" height: 50px; color: #fff; padding-top: 15px; background-color:  #1b6d85; text-align: center;">
                <p> Template @BITM</p>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>






