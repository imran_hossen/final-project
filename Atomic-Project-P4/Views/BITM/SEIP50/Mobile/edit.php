<?php 
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Mobile\Mobile;
$mobile=new Mobile();
$data =$mobile->prepare($_GET);
$themobile = $mobile->edit();
?>
<html>
    <head>
        <title>Update | Mobile Models</title>
    </head>
    <body>
        <fieldset>
            <legend>
                Crud of Mobile Models
            </legend> 
            <form action="update.php" method="POST">
                <label>Update Favorite Mobile Models</label><br/>
                <input type="hidden" name="id" value="<?php echo $themobile->id; ?>">
                <input type="text" name="title" id="title" value="<?php echo $themobile->title; ?>"><br/>
                <input type="submit" value="Update">
                <!--<input type="submit" value="Save & Enter Again">-->
<!--                <input type="reset" value="Reset">-->
            </form>
        </fieldset>
    <li><a href="index.php">Go To List </a></li>
    </body>
</html>