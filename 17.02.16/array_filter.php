<?php

$arr = array(
             0 => 'foo',
             1 => false,
             2 => -1,             //Filters elements of an array using a callback function
             3 => null,
             4 => ''
          );

print_r(array_filter($arr));
?>