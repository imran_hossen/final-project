<?php
session_start();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Birthday | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
        
        <form action="store.php" method="POST" >
            <fieldset>
                <legend>Add Birthday Information Here</legend>
                <label>Enter Your Full Name:</label>
                <input type="text" name="name">
                <span>
                    <?php 
                        if(isset($_SESSION['namerr'])){
                            echo $_SESSION['namerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter Your Birthday:</label>
                <input type="date" name="birthday">
                <span>
                    <?php 
                        if(isset($_SESSION['birthdayerr'])){
                            echo $_SESSION['birthdayerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter  Birthday Place:</label>
                <input type="text" name="b_place">
                <span>
                    <?php 
                        if(isset($_SESSION['b_placerr'])){
                            echo $_SESSION['b_placerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter Email Address:</label>
                <input type="text" name="email_address">
                <span>
                    <?php 
                        if(isset($_SESSION['emailerr'])){
                            echo $_SESSION['emailerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <label>Enter Mobile Number:</label>
                <input type="text" name="mobile">
                <span>
                    <?php 
                        if(isset($_SESSION['mobilerr'])){
                            echo $_SESSION['mobilerr'];
                            unset($_SESSION['']);
                        }
                    ?>
                </span>
                <br>
                <input type="submit" value="Save">
                <input type="reset" value="Reset">
            </fieldset>
        </form>
        
        
        
    </body>
</html>
