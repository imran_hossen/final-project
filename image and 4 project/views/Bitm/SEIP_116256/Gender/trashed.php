<?php

 include_once '../../../../vendor/autoload.php';
 
 use App\Bitm\SEIP_116256\Gender\gender;
 use App\Bitm\SEIP_116256\Utility\utility;
 
 $genderobj = new gender();
 $all_info = $genderobj->trashed();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Gender | Trashed Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="index.php">Home</a> 
        
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($all_info as $v_info){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_info['id'];?></td>
                    <td><?php echo $v_info['name'];?></td>
                    <td><?php echo $v_info['gender'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_info['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $v_info['id'];?>">Edit</a> |
                        <a href="restore.php?id=<?php echo $v_info['id'];?>">Restore</a> |
                        <a href="delete.php?id=<?php echo $v_info['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        
        
        
    </body>
</html>




