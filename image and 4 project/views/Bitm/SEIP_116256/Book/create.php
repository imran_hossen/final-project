<?php
session_start();
//var_dump($_SESSION);
?>
<html>
    <head>
        <title>
            Book | Create Page
        </title>
    </head>
    <body>
        <a href="index.php">Home Page</a>
        <form action="store.php" method="POST">
            <fieldset>
                <legend>Add Your Favourite Book</legend>
                <table>
                    <tr>
                        <td><label>Add New Book</label></td>
                        <td><input type="text" name="title" id="title"></td>
                        <td>
                            <?php 
                                if(isset($_SESSION['titlerr'])){
                                echo $_SESSION['titlerr'];
                                unset($_SESSION['titlerr']);
                                }
                            ?>
                        </td>
                </tr>
                <tr>
                    <td><label>Author Name</label></td>
                    <td><input type="text" name="author_name"></td>
                    <td>
                            <?php 
                                if(isset($_SESSION['autherr'])){
                                echo $_SESSION['autherr'];
                                unset($_SESSION['autherr']);
                                }
                            ?>
                    </td>
                </tr>
                <tr>
                    <td><label>Select a Image</label></td>
                    <td><input type="file" name="book_image"></td>
                    <td>
                            <?php 
                                if(isset($_SESSION['imagerr'])){
                                echo $_SESSION['imagerr'];
                                unset($_SESSION['imagerr']);
                                }
                            ?>
                    </td>
                </tr>
                <tr>
                <td><input type="submit" value="Save">
                    <input type="reset" value="Reset"></td>
                </tr>
                </table>
            </fieldset>
        </form>
    </body>
</html>
