<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116256\Book\BookClass_File;

$bookobj = new  BookClass_File();
$Alldata = $bookobj->trashed();

?>


<html>
    <head>
        <title>
            Book | Recycle Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New Book</a> |
        <a href="index.php">Home Page</a>
      
        <table border='1'>
            <th>SI</th>
            <th>ID</th>
            <th>Title</th>
            <th>Author Name</th>
            <th>Action</th>
            <?php
                $serial = 0;
                foreach($Alldata as $v_book){
                    $serial++;
//                    var_dump($v_book);
//                    exit();
            ?>
            <tr>
                <td><?php echo $serial;?></td>
                <td><?php echo $v_book["id"];?></td>
                <td><?php echo $v_book["title"];?></td>
                <td><?php echo $v_book["author_name"];?></td>
                <td>
                    <a href="show.php?id=<?php echo $v_book["id"];?>">View</a> | 
                    <a href="edit.php?id=<?php echo $v_book["id"];?>">Edit</a> | 
                    <a href="delete.php?id=<?php echo $v_book["id"];?>">Delete</a> |
                    <a href="restore.php?id=<?php echo $v_book["id"];?>">Restore</a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </body>
</html>
