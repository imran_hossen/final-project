<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_116256\Book\BookClass_File;

$bookobj = new  BookClass_File();
$id = $_GET['id'];
$ebook = $bookobj->viewSingleBook($id);

?>
<html>
    <head>
        <title>
            Book | Edit Page
        </title>
    </head>
    <body>
        <form action="update.php" method="POST">
            <fieldset>
                <legend>Edit Your Favourite Book</legend>
                <table>
                    <tr>
                        <td><label>Book Title</label></td>
                        <td>
                            <input type="text" name="title" id="title" value="<?php echo $ebook['title'];?>">
                            <input type="hidden" name="id" value="<?php echo $ebook['id'];?>">
                        </td>
                </tr>
                <tr>
                    <td><label>Author Name</label></td>
                    <td><input type="text" name="author_name" value="<?php echo $ebook['author_name'];?>"></td>
                </tr>
                
                <tr>
                <td><input type="submit" value="Update">
                    <input type="reset" value="Reset"></td>
                </tr>
                </table>
            </fieldset>
        </form>
    </body>
</html>
