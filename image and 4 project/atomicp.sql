-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2016 at 12:42 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicp`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `actor` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`id`, `title`, `actor`, `created_at`, `edited_at`, `deleted_at`) VALUES
(1, 'hhhh', 'imran', '2016-03-29 11:10:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'shahida', 'imran,prodip', '2016-03-29 11:17:49', '2016-03-29 13:14:53', '0000-00-00 00:00:00'),
(3, 'yyyyyyyyk', 'imran,sumon,prodip,mahfuz', '2016-03-29 11:17:55', '2016-03-29 11:53:59', '2016-03-29 13:14:27'),
(4, 'ppppppp', 'mahfuz', '2016-03-29 11:18:02', '0000-00-00 00:00:00', '2016-03-29 13:14:26'),
(5, 'iiii', 'imran,khurshed', '2016-03-29 11:19:49', '2016-03-29 11:54:06', '2016-03-29 13:14:24'),
(6, 'kjahjsad', 'prodip', '2016-03-29 11:54:14', '0000-00-00 00:00:00', '2016-03-29 13:14:24'),
(7, 'bangla', 'prodip', '2016-03-29 11:54:29', '0000-00-00 00:00:00', '2016-03-29 13:14:23'),
(8, 'gfggg', 'imran,mahfuz', '2016-03-29 13:14:46', '2016-03-29 13:14:58', '0000-00-00 00:00:00'),
(9, '01742347042', 'imran', '2016-03-29 13:17:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profilepic`
--

CREATE TABLE IF NOT EXISTS `profilepic` (
`id` int(11) NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profilepic`
--

INSERT INTO `profilepic` (`id`, `user_name`, `image_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(39, 'fffff', '1459678772eJyihEL.jpg', '2016-04-03 00:00:00', '2016-04-03 16:23:19', '0000-00-00 00:00:00'),
(41, 'jjjj', '1459678803eJyihEL.jpg', '2016-04-03 00:00:00', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actor`
--
ALTER TABLE `actor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepic`
--
ALTER TABLE `profilepic`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actor`
--
ALTER TABLE `actor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `profilepic`
--
ALTER TABLE `profilepic`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
