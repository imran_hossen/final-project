<?php
//Loop- while, for , foreach, do while

$a=1;
while($a<=10){
	echo $a.", ";
	$a++;
	
}

echo"<br><br>";

for($a=1;$a<=10;$a++){
	if($a%2==0){
		echo $a.".even<br>";
		
	}
	elseif($a%2!=0){
		echo $a.".odd <br>";
	}
	
}

echo"<br><br>";

/*foreach($array as $value);
foreach($array as $key => $value);

*/
$a=array(10,20,30,40);
foreach($a as $c){
	echo $c ."<br>";
}
echo"<br><br>";

$a=array(10,20,30,40);
foreach($a as $c=>$value){
	echo $c.".".$value."<br>";
}
echo"<br><br>";
?>