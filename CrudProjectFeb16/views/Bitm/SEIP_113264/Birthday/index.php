<?php

 include_once '../../../../vendor/autoload.php';
 
 use App\Bitm\SEIP_113264\Birthday\birthdayClass;
 use App\Bitm\SEIP_113264\Utility\utility;
 
 $birthdayobj = new birthdayClass();
 $all_info = $birthdayobj->view();
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Birthday | Home Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="trashed.php">Deleted Items</a> 
        <?php
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
              //  echo $_SESSION['message'];
                echo utility::message(NULL);
            }
        
        ?>
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Name</th>
              <th>Birthday</th>
              <th>Birthday Place</th>
              <th>Email Address</th>
              <th>Mobile Number</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($all_info as $v_info){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_info['id'];?></td>
                    <td><?php echo $v_info['name'];?></td>
                    <td><?php echo $v_info['birthday'];?></td>
                    <td><?php echo $v_info['b_place'];?></td>
                    <td><?php echo $v_info['email_address'];?></td>
                    <td><?php echo $v_info['mobile'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_info['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $v_info['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $v_info['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        
        
        
    </body>
</html>


