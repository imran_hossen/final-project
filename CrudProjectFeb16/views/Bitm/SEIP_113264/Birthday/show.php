<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Birthday\birthdayClass;

$birthdayobj = new birthdayClass();
$id = $_GET['id'];
$singleData = $birthdayobj->show($id);

 $data= $singleData['birthday'];
 
 $f_data = date('d-m-Y',  strtotime($data));
     //    echo '<pre>';
    //     print_r($f_data);
    //     exit();
 
?>

<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Details Page
        </title>
    </head>
    <body>
        <a href="index.php">Home</a>
       
        <table border="1">
            <thead>
              <th>ID</th>
              <th>Name</th>
              <th>Birthday</th>
              <th>Birthday Place</th>
              <th>Email Address</th>
              <th>Mobile Number</th>
              <th>Action</th>
            </thead>
            <tbody>
               <tr>
                    <td><?php echo $singleData['id'];?></td>
                    <td><?php echo $singleData['name'];?></td>
                    <td><?php echo $f_data;?></td>
                    <td><?php echo $singleData['b_place'];?></td>
                    <td><?php echo $singleData['email_address'];?></td>
                    <td><?php echo $singleData['mobile'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $singleData['id'];?>">View</a> |
                        <a href="edit.php?id=<?php echo $singleData['id'];?>">Edit</a> |
                        <a href="trash.php?id=<?php echo $singleData['id'];?>">Delete</a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        
        
    </body>
</html>


