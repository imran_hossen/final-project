<?php


include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Book\BookClass_File;



$id = $_GET['id'];

$bookobj = new  BookClass_File();
$SingleBook = $bookobj->viewSingleBook($id);
//echo '<pre>';
//print_r($SingleBook);
//exit();

?>
<html>
    <head>
        <title>
            Book | Show Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="index.php">Back to List</a>
        <table border='1'>
            <th>ID</th>
            <th>Title</th>
            <th>Author Name</th>
            <th>Action</th>
            
            <tr>
                <td><?php echo $SingleBook["id"];?></td>
                <td><?php echo $SingleBook["title"];?></td>
                <td><?php echo $SingleBook["author_name"];?></td>
                <td>
                    <a href="">Edit</a> | 
                    <a href="">Delete</a>
                </td>
            </tr>
        </table>
    </body>
</html>
