<?php

include_once '../../../../vendor/autoload.php';

use App\Bitm\SEIP_113264\Mobile\mobileClass;
use App\Bitm\SEIP_113264\Utility\utility;

$mobileobj = new mobileClass();
$mobile_info=$mobileobj->trashed();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Mobile | Trashed Page
        </title>
    </head>
    <body>
        <a href="create.php">Add New</a> |
        <a href="index.php">Home</a> 
        <?php
            if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
                echo $_SESSION['message'];
                echo utility::message();
            }
        
        ?>
        <table border="1">
            <thead>
              <th>SI</th>
              <th>ID</th>
              <th>Mobile Title</th>
              <th>Mobile Model</th>
              <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $s = 0;
                    foreach($mobile_info as $v_mobile){
                    $s++;    
                ?>
                <tr>
                    <td><?php echo $s;?></td>
                    <td><?php echo $v_mobile['id'];?></td>
                    <td><?php echo $v_mobile['title'];?></td>
                    <td><?php echo $v_mobile['model'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $v_mobile['id'];?>">View</a> |
                        <a href="restore.php?id=<?php echo $v_mobile['id'];?>">Restore</a> |
                        <a href="delete.php?id=<?php echo $v_mobile['id'];?>">Delete</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
       
        
         <?php 
                    foreach($mobile_info as $v_mobile){
                ?>
        <hr>
        
        <div>
            <h3>Mobile ID:<?php echo $v_mobile['id'];?></h3>
            <p>Mobile Title:<?php echo $v_mobile['title'];?></p>
            <p>Mobile Mobile:<?php echo $v_mobile['model'];?></p>
            <p>Deleted Time:
                <?php
                    $date = $v_mobile['deleted_at'];
                    
                    $d = date($date ,strtotime($date));
                    echo $d.'<br>';
                     echo date('D, d M Y h:i:s a', strtotime ($date));
                 
                ?>
            </p>
        </div>
        
        <?php }?>
        
    </body>
</html>
