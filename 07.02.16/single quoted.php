<?php

echo 'this is a simple string';

echo 'you can also have embeded newlines in strings this way as it is okay to do ';
//outputs: Arnold once said: "I\'ll be back" ';
echo 'Arnold once said: "I\'ll be back" ';
//outputs: you deleted C:\ * . *?
echo 'you deleted C:\ *. *?';

//outputs: variables do not $expend $either
echo 'variables do not $expend $either';

?>