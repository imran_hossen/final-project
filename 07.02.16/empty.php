<?php
//empty= determine wheather a variable is empty . bool empty (mixed $var)
/*
the following things are considered to be empty 

"" an empty string 
0 (o as an integer)
0.0(o as a float)
"0"(o as a sstring)
null
false
array ()(an empty array)
$var;(a variable declared , but without a value )
*/

$var = 0;
echo empty ($var);



?>