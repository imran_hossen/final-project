<?php
/*
//array_combine=2 array  same element 

$arr1=array("dhaka","capital","bangladesh");
$arr2=array(1,2,3);
$arr3=array_combine($arr1,$arr2);
echo "<pre>";
print_r($arr3);
echo "</pre>"    //note:(পিএইচপি array_combine() ফাংশন দিয়ে দুটি অ্যারে দিয়ে একটি অ্যারে বানানো হয়। একটি অ্যারের key এবং অপরটির মান (value) নিয়ে নতুন আরেকটি অ্যারে তৈরী করতে এই ফাংশন ব্যবহার হয়।)
*/

$arr1=array('c','c++');
$arr2=array(1,2);
$arr3=array_combine($arr1,$arr2);
echo "<pre>";
print_r($arr3);
echo "</pre>";
?>
