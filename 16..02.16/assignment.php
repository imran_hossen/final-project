<?php
/*
$a="i am imran 'khan'";
echo addslashes($a);//addslashes (input type=string, return type=string)
*/

/*
$a="bangladesh is a beautiful country";
print_r(explode(" ",$a));//explode (input type=string , return type=array)

*/
/*
$a=array('bangladesh','is','a','beautiful','country');
print_r(implode(" ",$a));//implode (input type=array, return type=string)
*/
/*
$a="bangladesh is a <b>beautiful</b> <i>country</i>";
echo htmlentities($a);//htmlentities(input type=any , return type= string)
*/

/*
$a="hello world";
echo trim($a,"he ");//trim (input type=string, return type= string)
*/

/*
$a="hello world";
echo ltrim($a, "h");//ltrim (input type=string, return type= string)
*/
/*
$a="hello world";
echo rtrim($a, " d");//rtrim (input type=string, return type= string)
*/

/*
$a="i am a basis \nstudent";//nl2br (input type= any/ string , return type= line break)
echo nl2br($a);

*/


/*
$a="hello world";
echo str_pad($a, 20,".");//str_pad(input type=string , return type= fixed length continue)

*/

/*
$a="hello";
echo str_repeat($a,10);//str_repeat(input type = string , return type=string continue )

*/


/*
$a="i am imran khan";
echo str_replace("khan","hossain",$a);//str_replace(input type= string , return type= replache word)
*/

/*
$a="imran khan";
print_r (str_split($a));//str_split(input type=string , return type=array)


$a="i am <b>imran</b> khan";
echo strip_tags($a);// strip_tags(input type = string , return type= Strips HTML and PHP tags from a string


/*
$a="bangladesh";
echo strlen($a);//strlen(input type= string , return type = length of string)
*/

/*
$a="IMRAN";
echo strtolower($a);//strtolower(input type= string , return type= convert to small letter)
*/

/*
$a="imran";
echo strtoupper($a);//strtoupper((input type= string , return type= convert to capital letter))

*/

/*
$a="bangladesh";
echo ucfirst($a);//ucfirst(input type=string , return type = first word will be capital)
*/

/*
$a="bangladesh";
echo strpos($a,"h");// strpos (input type = string , return type= give me character position)
*/


?>